<?php

require_once 'vendor/tpl.php';
require_once 'OrderLine.php';

$orderLines = [
    new OrderLine('Pen', 1, true),
    new OrderLine('Paper', 3, false),
    new OrderLine('Staples', 2, true)];

$data = [
    'sampleMessage' => 'Hello, templates!',
    'orderLines' => $orderLines,
    'colors' => ['red', 'blue', 'green'],
    'subTemplatePath' => 'ex3_sub_1.html',
    'currentYear' => date('Y')
];

print renderTemplate('templates/ex3_main.html', $data);
