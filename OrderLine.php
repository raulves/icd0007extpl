<?php

class OrderLine {

    public $productName;
    public $price;
    public $inStock;

    public function __construct($productName, $price, $inStock)
    {
        $this->productName = $productName;
        $this->price = $price;
        $this->inStock = $inStock;
    }


}
