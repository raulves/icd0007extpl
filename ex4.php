<?php

require_once 'vendor/tpl.php';
require_once 'Request.php';

$request = new Request($_REQUEST);

print $request; // display input parameters (for debugging)

$cmd = $request->param('cmd') ? $request->param('cmd') : 'ctf_form';

if ($cmd === 'ctf_form') {
    $data = [
        'title' => 'Celsius to Fahrenheit',
        'template' => 'ex4_form.html',
        'cmd' => 'ctf_calculate',
    ];

    print renderTemplate('templates/ex4_main.html', $data);

} else if ($cmd === 'ftc_form') {

    $data = [
        'title' => 'Fahrenheit to Celsius',
        'template' => 'ex4_form.html',
        'cmd' => 'ftc_calculate',
    ];

    print renderTemplate('templates/ex4_main.html', $data);

} else if ($cmd === 'ctf_calculate') {

    $input = intval($request->param('temperature'));
    $result = celsiusToFahrenheit($input);

    $message = "$input degrees in Celsius is $result degrees in Fahrenheit";

    $data = [
        'template' => 'ex4_result.html',
        'message' => $message
    ];

    print renderTemplate('templates/ex4_main.html', $data);

} else if ($cmd === 'ftc_calculate') {

    $input = intval($request->param('temperature'));
    $result = fahrenheitToCelsius($input);

    $message = "$input degrees in Fahrenheit is $result degrees in Celsius";

    $data = [
        'template' => 'ex4_result.html',
        'message' => $message
    ];

    print renderTemplate('templates/ex4_main.html', $data);

} else {
    throw new Error('programming error');
}

function fahrenheitToCelsius($temp) {
    return round(($temp - 32) / (9 / 5), 2);
}

function celsiusToFahrenheit($temp) {
    return round($temp * 9 / 5 + 32, 2);
}

