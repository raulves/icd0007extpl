<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Celsius to Fahrenheit Calculator</title>
</head>
<body>

<?php

    $input = $_GET['temp_in_celsius'];

    if (!is_numeric($input)) {
        $message = 'Temperature must be an integer!';
    }

    $result = toFahrenheit(intval($input));

    function toFahrenheit($temp) {
        return ($temp * 9 / 5) + 32;
    }

?>

<h3>Celsius to Fahrenheit Calculator</h3>

<strong>
    <?php print $input ?> in Fahrenheit is <?php print $result ?> degrees in Celsius
</strong>

<p>&copy; 2010-2019 </p>

</body>
</html>
